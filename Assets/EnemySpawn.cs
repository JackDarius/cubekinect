using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour 
{
    public GameObject Enemy;
    public Camera MainCamera;

    private CameraFollowSide _cameraFollowSide;

    void Start()
    {
        _cameraFollowSide = MainCamera.GetComponent<CameraFollowSide>();
    }

    void OnTriggerEnter(Collider other)
    {
        Instantiate(Enemy, new Vector3(transform.position.x + 2, transform.position.y + 3.4f, transform.position.z), Quaternion.identity);
        _cameraFollowSide.IsFighting = true;
    }
}
