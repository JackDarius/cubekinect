using UnityEngine;
using System.Collections;
using Windows.Kinect;

[System.Serializable]
public class BoundaryArm
{
    public float xMin, xMax, yMin, yMax;
}

public class ArmController : MonoBehaviour 
{
    public GameObject BodySourceManagerGo;
    public GameObject HandGo;
    public GameObject ForearmGo;
    public BoundaryArm Boundary;

    private BodySourceManager _bodySourceManager;

	void Start () 
    {
        _bodySourceManager = BodySourceManagerGo.GetComponent<BodySourceManager>();
	}
	
	void Update () 
    {
        Body[] data = _bodySourceManager.GetData();
        Body user = _bodySourceManager.getFirstTrackedUser();

        if (user == null) { return; }

        //Convertir position de la kinect (normalisée) dans Unity Space


        Debug.Log("HAND RIGHT POSITION : " + user.Joints[JointType.HandRight].GetJointPosition());
        HandGo.transform.position = new Vector3(user.Joints[JointType.HandRight].GetJointPosition().x * 10, user.Joints[JointType.HandRight].GetJointPosition().y * 5, 0.0f);
        ForearmGo.transform.position = new Vector3(user.Joints[JointType.ElbowRight].GetJointPosition().x * 10, user.Joints[JointType.ElbowRight].GetJointPosition().y * 5, 0.0f);
	}
}
