using UnityEngine;
using System.Collections;

public class CameraFollowSide : MonoBehaviour
{

    public GameObject Target;
    public bool IsFighting = false;

    private Transform _player;
    private Transform _enemyCurrent;
    private Vector3 _offset;
    private float _currentHeight;
    private float _currentWidth;

    void Start()
    {
        _player = Target.transform;
        _offset = _player.position - transform.position;
        _currentHeight = transform.position.y;
        _currentWidth = transform.position.x;
    }

    void LateUpdate()
    {
        Vector3 current;
        Vector3 pointOfView;
        float desiredAngle = Target.transform.eulerAngles.z;
        Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);

        if(IsFighting)
        {
            _enemyCurrent = GameObject.FindGameObjectWithTag("Enemy").transform;
            pointOfView = new Vector3(_player.position.x + 5, _player.position.y, _player.position.z);
        }
        else
            pointOfView = new Vector3(_player.position.x, _player.position.y, _player.position.z);
        
        current = _player.position - (rotation * _offset);
        _currentHeight = Mathf.Lerp(_currentHeight, _player.position.y + 3, Time.deltaTime * 0.5f);

        transform.position = new Vector3(current.x, _currentHeight, current.z);
        transform.LookAt(pointOfView);
    }
}