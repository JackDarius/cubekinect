using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;

public class PlayerController : MonoBehaviour 
{
    public GameObject BodySourceManagerGo;
    public float Speed;
    public float Threshold;

    private BodySourceManager _bodySourceManager;
    private float _angleArmShoulderInitial = 0f;
    private float _gestureSum = 0f;
    private float _step;
    private List<float> _degreePosition;
    private bool _isReturn = false;
    private Vector3 _target;

	void Start () 
    {
        _bodySourceManager = BodySourceManagerGo.GetComponent<BodySourceManager>();
        _target = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        _degreePosition = new List<float>();
        _degreePosition.Add(0f);
	}
	
	void Update () 
    {
        //Recupere le premier utilisateur synchronise
        Body[] data = _bodySourceManager.GetData();
        Body user = _bodySourceManager.getFirstTrackedUser();

        if (user == null) { return; }

        _step = Speed * Time.deltaTime;

        //Calcul des axes du plan referentiel du torse
        Vector3 right = user.Joints[JointType.ShoulderLeft].GetJointPosition() -
                        user.Joints[JointType.ShoulderRight].GetJointPosition();
        Vector3 up = user.Joints[JointType.SpineShoulder].GetJointPosition() -
                     user.Joints[JointType.SpineBase].GetJointPosition();
        Vector3 forward = Vector3.Cross(right, up); //Left Hand Rule

        //Calcul de la direction du bras
        Vector3 armRight = user.Joints[JointType.ShoulderRight].GetJointPosition() -
                           user.Joints[JointType.ElbowRight].GetJointPosition();
        
        //Calcul de la direction de l'avant bras
        Vector3 forearmRight = user.Joints[JointType.HandRight].GetJointPosition() -
                          user.Joints[JointType.ElbowRight].GetJointPosition();

        //Calcul de l'angle initial entre le bras collé à son flanc et la direction des épaules
        if(_angleArmShoulderInitial == 0)
            _angleArmShoulderInitial = Vector3.Angle(armRight, right);

        //Calcul de l'angle entre l'avant-bras et le bras
        float angleForearmArm = Vector3.Angle(forearmRight, armRight);

        //Calcul de l'angle courant entre le bras et la direction des épaules
        float angleArmShoulderCurrent = Vector3.Angle(armRight, right);

        //Calcul de l'angle entre la direction des épaules et de l'avant bras
        float angleForearmShoulder = Vector3.Angle(forearmRight, right);

        //Test si l'utilisateur plaque bien son bras contre son flanc
        if(angleArmShoulderCurrent > _angleArmShoulderInitial - 15 && angleArmShoulderCurrent < _angleArmShoulderInitial + 15)
        {
            //Test si l'avant bras est bien à angle droit du bras
            if (angleForearmArm > 75 && angleForearmArm < 105)
            {
                //Test si l'utilisateur fait le bon mouvement avec son avant bras
                if (angleForearmShoulder > 0 && angleForearmShoulder < 100)
                {
                    if (!_degreePosition.Contains(((int)angleForearmShoulder)) && _degreePosition[_degreePosition.Count] < (int)angleForearmShoulder) 
                    {
                        _degreePosition.Add((int)angleForearmShoulder);
                        _gestureSum++;
                        //Debug.Log("ANGLE FOREARMSHOULDER : " + (int)angleForearmShoulder + " GESTURE SUM : " + _gestureSum);
                    }
                    //Test si j'ai mon bras collé au ventre alors mon allez est ok
                    if ((angleForearmShoulder > 0 && angleForearmShoulder < 30) && !_isReturn && _gestureSum >= Threshold)
                    {
                        _degreePosition.Clear();
                        _isReturn = true;
                        _gestureSum = 0f;
                    }
                    else if ((angleForearmShoulder < 95 && angleForearmShoulder > 80) && _isReturn && _gestureSum >= Threshold) //Test si je suis bien à angle droit de mon ventre et que j'ai bien fait mon allez
                    {
                        _degreePosition.Clear();
                        _target = new Vector3(transform.position.x + 3, transform.position.y, transform.position.z); //Nouvelle position pour mon player 
                        _gestureSum = 0f;
                        _isReturn = false;
                    }
                }
            }
        }
        /*if (_degreePosition != null)
        {
            for (int i = 1; i <= _degreePosition.Count; i++)
                Debug.Log("INDEX " + i + " : " + _degreePosition.IndexOf(_degreePosition[i]));
        }*/
        if (_target != Vector3.zero)
        {
            if (transform.position != _target)
                transform.position = Vector3.MoveTowards(transform.position, _target, _step);
            else
                _target = Vector3.zero;
        }
	}
}
