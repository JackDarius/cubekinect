using UnityEngine;
using System.Collections;
using System.Linq;
using Windows.Kinect;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, yMin, yMax;
}

public class CubeController : MonoBehaviour 
{
    public GameObject PlaneGo;
    public GameObject BodySourceManagerGo;
    public float Speed;
    public Boundary Boundary;

    private BodySourceManager _bodySourceManager;

	void Start () 
    {
        _bodySourceManager = BodySourceManagerGo.GetComponent<BodySourceManager>();
	}
	
	void Update () 
    {
        //Limite de la position de mon bloc (boundary)
        transform.position = new Vector3
        (
            Mathf.Clamp(GetComponent<Rigidbody>().position.x, Boundary.xMin, Boundary.xMax),
            Mathf.Clamp(GetComponent<Rigidbody>().position.z, Boundary.yMin, Boundary.yMax),
            0.0f
        );

        Body[] data = _bodySourceManager.GetData();
        Body user = _bodySourceManager.getFirstTrackedUser();

        if (user == null) { return; }
        
        //Calcul des axes du plan referentiel du torse
        Vector3 right = user.Joints[JointType.ShoulderLeft].GetJointPosition() -
                        user.Joints[JointType.ShoulderRight].GetJointPosition();
        Vector3 up = user.Joints[JointType.SpineShoulder].GetJointPosition() -
                     user.Joints[JointType.SpineBase].GetJointPosition();
        Vector3 forward = Vector3.Cross(right, up); //Left Hand Rule


        PlaneGo.transform.rotation = Quaternion.LookRotation(up, forward); // Plan Referentiel

        //Calcul de l'axe du bras
        Vector3 armRightAxis = user.Joints[JointType.HandRight].GetJointPosition() - 
                               user.Joints[JointType.ShoulderRight].GetJointPosition();
        Vector3 armLeftAxis = user.Joints[JointType.HandLeft].GetJointPosition() -
                               user.Joints[JointType.ShoulderLeft].GetJointPosition();

        //transform.position = user.Joints[JointType.HandRight].GetJointPosition();

        //Calcul angle entre le bras et le torse
        float angle = Vector3.Angle(up, armRightAxis);

        //Mouvement de mon bloc
        float moveVertical = user.Joints[JointType.HandRight].Position.Y;
        float moveHorizontal = user.Joints[JointType.ElbowRight].Position.X;
 
        if(angle < 180 && angle > 0)
        {
            Vector3 movement = new Vector3(0.0f, moveVertical * Boundary.yMax, 0.0f);
            transform.Translate(movement);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        Die();
    }

    void Die()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}

//Extension pour la librairie Kinect
public static class KinectController
{
    public static Vector3 GetJointPosition(this Windows.Kinect.Joint self)
    {
        return new Vector3(self.Position.X, self.Position.Y, self.Position.Z);
    }
}
