using UnityEngine;
using System.Collections;

public class Generate : MonoBehaviour {

    public GameObject Block;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("CreateObstacle", 1f, 3.5f);
    }

    void CreateObstacle()
    {
        Instantiate(Block);
    }
}
