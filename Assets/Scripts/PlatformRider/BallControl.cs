using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class BallControl : MonoBehaviour 
{
	public float acceleration = 10f;
    public GameObject BodySourceManagerGo;

    public GameObject AlarmBadPostureImg;
    public GameObject TextBadPostureImg;

    private BodySourceManager _bodySourceManager;
	private bool isGrounded = true;
    private bool isRight = false;
    private bool isLeft = false;

    void Start()
    {
        AlarmBadPostureImg.SetActive(false);
        TextBadPostureImg.SetActive(false);

        _bodySourceManager = BodySourceManagerGo.GetComponent<BodySourceManager>();
    }

    void Update()
    {
        //Recupere le premier utilisateur synchronise
        Body[] data = _bodySourceManager.GetData();
        Body user = _bodySourceManager.getFirstTrackedUser();

        if (user == null) { return; }

        //Calcul des axes du plan referentiel du torse
        Vector3 right = user.Joints[JointType.ShoulderLeft].GetJointPosition() -
                        user.Joints[JointType.ShoulderRight].GetJointPosition();
        Vector3 up = user.Joints[JointType.SpineShoulder].GetJointPosition() -
                     user.Joints[JointType.SpineBase].GetJointPosition();
        Vector3 forward = Vector3.Cross(right, up); //Left Hand Rule

        //Calcul de l'axe du bras
        Vector3 armRightAxis = user.Joints[JointType.HandRight].GetJointPosition() -
                               user.Joints[JointType.ShoulderRight].GetJointPosition();
        Vector3 forearmRightAxis = user.Joints[JointType.HandRight].GetJointPosition() -
                               user.Joints[JointType.ElbowRight].GetJointPosition();
        Vector3 armLeftAxis = user.Joints[JointType.HandLeft].GetJointPosition() -
                               user.Joints[JointType.ShoulderLeft].GetJointPosition();

        //Calcul angle entre le bras et le torse
        float angleHorizontal = Vector3.Angle(right, armRightAxis);
        float angleVertical = Vector3.Angle(up, armRightAxis);

        float angleArmForearm = Vector3.Angle(armRightAxis, forearmRightAxis);

        if (angleArmForearm < 15) //Test si le bras est bien droit avec une marge d'erreur de 15 degres
        {
            if (AlarmBadPostureImg.activeInHierarchy)
                AlarmBadPostureImg.SetActive(false);
            if (TextBadPostureImg.activeInHierarchy)
                TextBadPostureImg.SetActive(false);
            

            if (angleVertical > 70 && angleVertical < 110) //Test si mon bras est bien en face de moi (à Hauteur de mon épaule droite)
            {
                if (angleHorizontal > 110 && angleHorizontal < 180) 
                    isRight = true;
                if (angleHorizontal < 70 && angleVertical > 0)
                    isLeft = true;
                else if (angleHorizontal > 70 && angleHorizontal < 110)
                {
                    isRight = false;
                    isLeft = false;
                }
            }
        }
        else
        {
            if (!AlarmBadPostureImg.activeInHierarchy)
                AlarmBadPostureImg.SetActive(true);
            if (!TextBadPostureImg.activeInHierarchy)
                TextBadPostureImg.SetActive(true);
        }

        //Controle de la balle gauche/droite
        if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.LeftArrow))
            isLeft = true;
        else if(Input.GetKeyUp(KeyCode.Q) || Input.GetKeyUp(KeyCode.LeftArrow))
            isLeft = false;

        if(Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            isRight = true;
        else if(Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
            isRight = false;
    }

	void FixedUpdate () 
	{
		if (isGrounded)
			transform.GetComponent<Rigidbody>().AddForce(Vector3.right * acceleration);
		else
			transform.GetComponent<Rigidbody>().AddForce(Vector3.right * 1f);
		
		if (isLeft)
			transform.GetComponent<Rigidbody>().AddForce(0f, 0f, 5f);
		else if (isRight)
			transform.GetComponent<Rigidbody>().AddForce(0f, 0f, -5f);
	}
	
	void OnCollisionEnter(Collision other)
	{
		isGrounded = true;
	}
	
	void OnCollisionExit(Collision other)
	{
		isGrounded = false;
	}
}
