using UnityEngine;
using System.Collections;

public class CameraFollowBehind : MonoBehaviour {
	
	public GameObject target;
	
	private Transform rig;
	private Vector3 offset;
	private bool moveUp;
	private float desiredHeight;
	private float currentHeight;
	
	void Start () 
	{
		rig = target.transform.FindChild("Sphere");
		offset = rig.position - transform.position;
		currentHeight = transform.position.y;
	}
	
	void OnTriggerEnter(Collider other)
	{
		desiredHeight = transform.position.y + 2;
		moveUp = true;
	}
	
	void LateUpdate () 
	{
		float desiredAngle = target.transform.eulerAngles.z;
		
		if (moveUp)
		{
			currentHeight = Mathf.Lerp(currentHeight, desiredHeight, Time.deltaTime);
			
			if (Mathf.Abs(currentHeight - desiredHeight) < 1f)
				moveUp = false;
		}
		else
		{
			currentHeight = Mathf.Lerp(currentHeight, rig.position.y, Time.deltaTime * 0.5f);
		}
		
		Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);
		
		Vector3 current = rig.position - (rotation * offset);
		transform.position = new Vector3(current.x, currentHeight, current.z); 
		transform.LookAt(rig);
	}
}